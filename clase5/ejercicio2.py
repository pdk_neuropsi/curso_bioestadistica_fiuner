# https://www.marsja.se/repeated-measures-anova-using-python/
# https://danvatterott.com/blog/2016/02/28/repeated-measures-anova-in-python-kinda/

from pyvttbl import DataFrame
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats

data = pd.read_excel('ANOVA_un_factor_medidas_repetidas.xlsx')
data_transf=pd.DataFrame(columns=['Sujeto','Droga','Dolor'])
for i in range(len(data)):
	for col in data.columns:
		data_transf = data_transf.append({'Sujeto':int(i),'Droga':str(col),'Dolor':data[col].iloc[i]},ignore_index=True)
data_transf.loc[:,'Sujeto']=data_transf.loc[:,'Sujeto'].astype(int)
data1 = DataFrame(data_transf)
anov = data1.anova(dv='Dolor',sub='Sujeto',wfactors=['Droga'])
print anov