from pyvttbl import DataFrame
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats
from scipy.stats import mstats
from nemeyi import kw_nemenyi
data = pd.read_excel('ANOVA_un_factor_muestras_independientes.xlsx')

data_transf=pd.DataFrame(columns=['Marca','Satisfaccion'])
for i in range(len(data)):
	for col in data.columns:
		data_transf = data_transf.append({'Marca':str(col),'Satisfaccion':data[col].iloc[i]},ignore_index=True)

data1 = DataFrame(data_transf)

Marcas = ['Ramses','Sheik','Trojan','Unnamed']

kw_result = mstats.kruskalwallis(data[Marcas[0]].values,data[Marcas[1]].values,data[Marcas[2]].values,data[Marcas[3]].values)

p_kw = kw_result[1]

if p_kw<0.05:
	print('El p-value de Kuskal-Wallis es menor a 0.05. Es necesario hacer pruebas Post Hoc. (P=%f)' %p_kw)
else:
	print('El p-value es mayor a 0.05. TODAS las series parecen no diferir en su mediana.')

if p_kw<0.05:
	permutations = [(0,1),(0,2),(0,3),(1,2),(1,3),(2,3)]
	post_hoc = kw_nemenyi([data['Ramses'].values,data['Sheik'].values,data['Trojan'].values,data['Unnamed'].values],
						  to_compare=permutations)
	print post_hoc
	for idx,test in enumerate(post_hoc[3]):
		if test:
			print '-------------------------------------------------------------------------------'
			print '-------------------------------------------------------------------------------'
			print 'La prueba de %s con %s fue significativa con un p =%f.' %(Marcas[permutations[idx][0]],
																			 Marcas[permutations[idx][1]],
																			 post_hoc[2][idx])

