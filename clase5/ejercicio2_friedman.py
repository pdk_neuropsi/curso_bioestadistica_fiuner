# https://www.marsja.se/repeated-measures-anova-using-python/
# https://danvatterott.com/blog/2016/02/28/repeated-measures-anova-in-python-kinda/

from pyvttbl import DataFrame
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats
from nemeyi import kw_nemenyi

data = pd.read_excel('ANOVA_un_factor_medidas_repetidas.xlsx')
data_transf=pd.DataFrame(columns=['Sujeto','Droga','Dolor'])
for i in range(len(data)):
	for col in data.columns:
		data_transf = data_transf.append({'Sujeto':int(i),'Droga':str(col),'Dolor':data[col].iloc[i]},ignore_index=True)
data_transf.loc[:,'Sujeto']=data_transf.loc[:,'Sujeto'].astype(int)
data1 = DataFrame(data_transf)

Drogas = ['Entrophen','Tylenol','Motrin']
friedman = stats.friedmanchisquare(data['Entrophen'],data['Tylenol'],data['Motrin'])
print friedman
p_kw = friedman[1]
if p_kw<0.05:
	print('El p-value de Kuskal-Wallis es menor a 0.05. Es necesario hacer pruebas Post Hoc. (P=%f)' %p_kw)
else:
	print('El p-value es mayor a 0.05. TODAS las series parecen no diferir en su mediana.')

if p_kw<0.05:
	permutations = [(0,1),(0,2),(1,2)]
	post_hoc = kw_nemenyi([data['Entrophen'].values,data['Tylenol'].values,data['Motrin'].values],
						  to_compare=permutations,method="tukey")
	print post_hoc
	for idx,test in enumerate(post_hoc[3]):
		if test:
			print '-------------------------------------------------------------------------------'
			print '-------------------------------------------------------------------------------'
			print 'La prueba de %s con %s fue significativa con un p =%f.' %(Drogas[permutations[idx][0]],
																			 Drogas[permutations[idx][1]],
																			 post_hoc[2][idx])

