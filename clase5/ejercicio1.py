from pyvttbl import DataFrame
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats

data = pd.read_excel('ANOVA_un_factor_muestras_independientes.xlsx')

data_transf=pd.DataFrame(columns=['Marca','Satisfaccion'])
for i in range(len(data)):
	for col in data.columns:
		data_transf = data_transf.append({'Marca':str(col),'Satisfaccion':data[col].iloc[i]},ignore_index=True)

data1 = DataFrame(data_transf)
anova = data1.anova1way(val='Satisfaccion',factor='Marca')
print '------------------------------------------------------------------------'
print 'USANDO pyvttbl'

print anova

from statsmodels.stats.multicomp import pairwise_tukeyhsd, MultiComparison

import statsmodels.api as sm
from statsmodels.formula.api import ols

mod = ols('Satisfaccion ~ Marca',data=data_transf).fit()

aov_table = sm.stats.anova_lm(mod, typ=3)

print '------------------------------------------------------------------------'
print '------------------------------------------------------------------------'
print '------------------------------------------------------------------------'
print '------------------------------------------------------------------------'

print 'USANDO StatsModels'

print '------------------------------------------------------------------------'
print '------------------------------------------------------------------------'
print 'Modelo de regresion lineal Satisfaccion ~ Marca'
print mod.summary()

print 'Resultado del ANOVA'
print aov_table
print 'Resultado del Turkey HSD'

tukey = pairwise_tukeyhsd(data_transf['Satisfaccion'],data_transf['Marca'])
print '------------------------------------------------------------------------'
print '------------------------------------------------------------------------'
print tukey
print '------------------------------------------------------------------------'
print '------------------------------------------------------------------------'

plt.figure(1)
sns.boxplot(x='Marca',y='Satisfaccion',saturation=0.60,data=data_transf)
sns.swarmplot(x='Marca',y='Satisfaccion',data=data_transf,color='0.25',size=10)
plt.title('Distribucion de la Satisfaccion con la Marca')
plt.ylabel('Satisfaccion')
plt.xlabel('Marca')


res = mod.resid # residuals
fig = sm.qqplot(res,stats.t,fit=True,line='45')
plt.show()

data_transf.insert(2,'Residuo',res)
