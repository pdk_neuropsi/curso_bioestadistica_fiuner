
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import scipy.stats as stats

sns.set_palette('colorblind')
data_file='data_connections_dft_vs_con.csv'
data = pd.read_csv(open(data_file,'r'))



CTLS = data.loc[data['DIAG']=='CON']
DFTS = data.loc[data['DIAG']=='DFT']

for con in np.unique(data['CONNECTION']):
	vals_ctrls = CTLS.loc[CTLS['CONNECTION'] == con]['value'].values
	vals_dft = DFTS.loc[DFTS['CONNECTION'] == con]['value'].values
	if con == 'con_rh-superiortemporal_lh-caudalanteriorcingulate':
		ctrls_norm_kstest = stats.kstest(vals_ctrls,'norm')
		dfts_norm_kstest = stats.kstest(vals_dft,'norm')

		ctrls_norm_shastest = stats.shapiro(vals_ctrls)
		dfts_norm_shatest = stats.shapiro(vals_dft)

		ctrl_dft_var_levene = stats.levene(vals_ctrls,vals_dft)

		t, p = stats.ttest_ind(vals_ctrls,vals_dft,equal_var=True)


print 'Komodorov-Smirnov test'
print'Test de normalidad controles'
print ctrls_norm_kstest
print'Test de normalidad DFT'
print dfts_norm_kstest

print 'Shapiro test'
print'Test de normalidad controles'
print ctrls_norm_shastest
print'Test de normalidad DFT'
print dfts_norm_shatest

print 'Test de varianzas de Levene'
print ctrl_dft_var_levene

print 'Test Parametrico de pruebas no apareadas'
print 'T val: %f.2 p-val: %f.2' %(t,p)


int_conf_ctrls = stats.t.interval(0.95, len(vals_ctrls)-1, loc=np.mean(vals_ctrls), scale=stats.sem(vals_ctrls))
int_conf_dfts = stats.t.interval(0.95, len(vals_dft)-1, loc=np.mean(vals_dft), scale=stats.sem(vals_dft))

print "Intervalos de confianzas"
print 'Controles'
print int_conf_ctrls
print 'DFT'
print int_conf_dfts