
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

sns.set_palette('colorblind')
data_file='data_connections_dft_vs_con.csv'
data = pd.read_csv(open(data_file,'r'))



CTLS = data.loc[data['DIAG']=='CON']
DFTS = data.loc[data['DIAG']=='DFT']

for con in np.unique(data['CONNECTION']):
	plt.figure(dpi=100)
	plt.title(con)
	bins = np.linspace(-3, 3, 12)
	plt.hist(CTLS.loc[CTLS['CONNECTION']==con]['value'].values,label='CTRLS',bins=bins,alpha=0.6)
	plt.hist(DFTS.loc[DFTS['CONNECTION'] == con]['value'].values, label='DFTS',bins=bins,alpha=0.6)
	plt.legend()
	plt.ylim([0,21])
	plt.ylabel('Frecuency')
	plt.xlabel('Connections strength')
	plt.savefig(con+'.png')
	plt.close()

