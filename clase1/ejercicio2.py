import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd


def read_data(csv_file):
    data = pd.read_csv(open(csv_file,'r'))
    data = data.sort('Umbral')
    return data

def add_classification(data):
    classification = []
    for i in range(0,len(data)):
        if data['Umbral'].values[i]<=5:
            classification.append('Bajo')
        elif data['Umbral'].values[i]<=10:
            classification.append('Medio')
        elif data['Umbral'].values[i]<=15:
            classification.append('Alto')
        else:
            classification.append('Muy alto')

    data.insert(1,'Clasificacion',classification)
    return data

data = read_data('data/Exercise_2_data.csv')
data = add_classification(data)
sns.set_style('whitegrid')
print data


plt.figure(1,figsize=(8,6))
sns.distplot(data['Umbral'], kde=True, rug=True)
plt.title('Distribucion de valores de umbral')
plt.xlabel('Umbral [mA]')
plt.ylabel('Frecuencia')


plt.figure(2,figsize=(8,6))

data_to_plot = pd.melt(data, "Clasificacion", var_name="Umbral")
sns.swarmplot(x="Clasificacion", y="value", hue="Clasificacion", data=data_to_plot)
plt.title('Distribucion de valores de umbral')
plt.ylabel('Umbral [mA]')
plt.xlabel('Clasificacion')
plt.show()