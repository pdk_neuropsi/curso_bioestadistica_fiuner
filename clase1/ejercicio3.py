import pandas as pd


def read_data(csv_file):
    data = pd.read_csv(open(csv_file,'r'))
    return data

data = read_data('data/Exercise_3_data.csv')

print 'Tabla de contingencia Obs A - Obs B'
col1 = 'Obs A'
col2 = 'Obs B'
print pd.crosstab(data[col1].values>0, data[col2].values>0,rownames=[col1],colnames=[col2])


print 'Tabla de contingencia Obs A - Obs C'
col1 = 'Obs A'
col2 = 'Obs C'
print pd.crosstab(data[col1].values>0, data[col2].values>0,rownames=[col1],colnames=[col2])

print 'Tabla de contingencia Obs A - Obs D'
col1 = 'Obs A'
col2 = 'Obs D'
print pd.crosstab(data[col1].values>0, data[col2].values>0,rownames=[col1],colnames=[col2])