import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from scipy.stats import ttest_rel
import numpy as np

def read_data(csv_file):
    data = pd.read_csv(open(csv_file,'r'))
    return data

data = read_data('data/Exercise_4_data_reformat.csv')

plt.figure(1)
var = "Reflex threshold"
sub_data=data.loc[(data['Variable']==var)]
sub_data = sub_data.sort_values('Subject')
t,p1 = ttest_rel(np.array(sub_data[sub_data["Time"] == 'Before']['Value']),np.array(sub_data[sub_data['Time'] == 'After']['Value']))
sns.boxplot(x="Variable", y="Value", hue="Time", data=data.loc[(data['Variable']==var)])
plt.title('%s antes y despues de la aplicacion de un analgesico (p=%0.5f)' % (var,p1))
plt.ylabel(var)


plt.figure(2)
var = "Pain threshold"
sub_data=data.loc[(data['Variable']==var)]
sub_data = sub_data.sort_values('Subject')
t,p1 = ttest_rel(np.array(sub_data[sub_data["Time"] == 'Before']['Value']),np.array(sub_data[sub_data['Time'] == 'After']['Value']))
sns.boxplot(x="Variable", y="Value", hue="Time", data=data.loc[(data['Variable']==var)])
plt.title('%s antes y despues de la aplicacion de un analgesico (p=%0.5f)' % (var,p1))
plt.ylabel(var)






data = read_data('data/Exercise_4_data.csv')

sub_data_before=sub_data=data.loc[(data['Time']=='Before')]
sub_data_after=sub_data=data.loc[(data['Time']=='After')]
sub_data_before = sub_data_before.sort_values('Subject')
sub_data_after = sub_data_after.sort_values('Subject')
plt.figure(3)
ax = plt.subplot(111)

for i in sub_data_after['Subject'].values:
    x1 = sub_data_before["Reflex threshold"].loc[sub_data_before['Subject']==i].values
    x2 = sub_data_after["Reflex threshold"].loc[sub_data_after['Subject']==i].values
    y1 = sub_data_before['Pain threshold'].loc[sub_data_before['Subject'] == i].values
    y2 = sub_data_after['Pain threshold'].loc[sub_data_after['Subject'] == i ].values
    plt.plot([x1,x2],[y1,y2],alpha=0.8)

sns.regplot(data=sub_data_before,x="Reflex threshold",y='Pain threshold',marker='*',ax=ax,fit_reg=False)
sns.regplot(data=sub_data_after,x="Reflex threshold",y='Pain threshold',marker='+',ax=ax,fit_reg=False)








var = "Reflex threshold"
reflex_relative = sub_data_before[var].values-sub_data_after[var].values
reflex_relative= np.divide(reflex_relative,sub_data_before[var].values)*100
var = "Pain threshold"
pain_relative = sub_data_before[var].values-sub_data_after[var].values
pain_relative= np.divide(pain_relative,sub_data_before[var].values)*100
data_reordered=pd.DataFrame()
data_reordered.insert(0,'Subject',sub_data_before['Subject'].values)
data_reordered.insert(1,"Cambio relativo del umbral de reflejo(%)",reflex_relative)
data_reordered.insert(2,"Cambio relativo del umbral al dolor(%)",pain_relative)
#sns.pairplot(data_reordered,x_vars="Relative reflex threshold change",y_vars="Relative pain threshold change")

sns.jointplot(x="Cambio relativo del umbral de reflejo(%)",y="Cambio relativo del umbral al dolor(%)",data=data_reordered,stat_func=None)

#plt.ylabel("Cambio relativo del umbral al dolor(%)")
#plt.xlabel("Cambio relativo del umbral de reflejo(%)")

plt.show()