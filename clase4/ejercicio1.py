
import pandas as pd
import scipy.stats as stats

filename='Wilcoxon_1_muestra.xlsx'
data = pd.read_excel(filename)
mean=7.0
data.insert(0,'Mean',mean)
st1 = stats.wilcoxon(data['Umbral reflejo (mA)'],data['Mean'])

print st1

if st1[1]<0.05:
	print 'La diferencia es significativa, por lo tanto la media no es %.2f' %mean
else:
	print 'No se puede descartar la hipotesis nula, por lo tanto la media puede ser %.2f' %mean

