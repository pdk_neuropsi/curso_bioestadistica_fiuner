import pandas as pd
import scipy.stats as stats
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
filename='Mann_Whitney_U_2_muestras_independientes.xlsx'
data = pd.read_excel(filename)
campos=['PPT_mujeres','PPT_hombres']
st= stats.mannwhitneyu(data[campos[0]],data[campos[1]],alternative='two-sided')

print data.describe()
print st
if st[1]<0.05:
	print 'La diferencia es significativa. Por lo tanto ambas muestras tienen de distintas medias.'
else:
	print 'No se puede descartar la hipotesis nula, por lo tanto las medias pueden ser iguales.'


