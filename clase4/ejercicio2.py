

import pandas as pd
import scipy.stats as stats
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
filename='Wilcoxon_2_muestras_pareadas.xlsx'
data = pd.read_excel(filename)
st = stats.wilcoxon(data['Umbral_RR_1'],data['Umbral_RR_2'])

mediana1= np.median(data['Umbral_RR_1'])
mediana2 = np.median(data['Umbral_RR_2'])

print 'Mediana de data Umbral_RR_1 es : %.5f' %mediana1
print 'Mediana de data Umbral_RR_2 es : %.5f' %mediana2
print 'La diferencia de medianas es : %.5f' %(abs(mediana1-mediana2))

print st
plot= False
if plot:
	plt.figure()
	ax1 = plt.subplot(131)
	ax2 = plt.subplot(132)
	ax3 = plt.subplot(133)

	sns.distplot(data['Umbral_RR_1'],ax=ax1,label='Umbral_RR_1')
	sns.distplot(data['Umbral_RR_2'],ax=ax2,label='Umbral_RR_2')

	sns.distplot(data['Umbral_RR_1'],ax=ax3,label='Umbral_RR_1')
	sns.distplot(data['Umbral_RR_2'],ax=ax3,label='Umbral_RR_2')

	plt.show()

if st[1]<0.05:
	print 'La diferencia es significativa. Por lo tanto ambas muestras tienen de distintas medias.'
else:
	print 'No se puede descartar la hipotesis nula, por lo tanto las medias pueden ser iguales.'


nt1 = stats.shapiro(data['Umbral_RR_1'])
nt2 = stats.shapiro(data['Umbral_RR_2'])

print('Los test de normalidad dan :')
print('Umbral_RR_1: ' + str(nt1))
print('Umbral_RR_2: ' + str(nt2))


st2 = stats.ttest_rel(data['Umbral_RR_1'],data['Umbral_RR_2'])

print('Significancia usando la prueba t-test para muestras pariadas ' + str(st2))