# -*- coding: utf-8 -*-
import pandas as pd
import scipy.stats as stats
import statsmodels.api as sm
from statsmodels.formula.api import ols
from statsmodels.stats.anova import anova_lm
import seaborn as sns
from statsmodels.graphics.factorplots import interaction_plot
import matplotlib.pyplot as plt

def eta_squared(aov):
	aov['eta_sq'] = 'NaN'
	aov['eta_sq'] = aov[:-1]['sum_sq'] / sum(aov['sum_sq'])
	return aov


def omega_squared(aov):
	mse = aov['sum_sq'][-1] / aov['df'][-1]
	aov['omega_sq'] = 'NaN'
	aov['omega_sq'] = (aov[:-1]['sum_sq'] - (aov[:-1]['df'] * mse)) / (sum(aov['sum_sq']) + mse)
	return aov

filename='ANOVA_factorial.xlsx'
data = pd.read_excel(filename)

formula = 'Satisfaccion ~ C(Condicion) + C(Marca) + C(Condicion):C(Marca)'

model = ols(formula, data).fit()
aov_table = anova_lm(model, typ=3)

#fig = interaction_plot(data.Condicion, data.Marca, data.Satisfaccion, ms=10)




eta_squared(aov_table)
omega_squared(aov_table)
print(aov_table)

plt.figure()
#plt.subplot2grid(())
ax1=plt.subplot(121)
sns.pointplot(x="Condicion", y="Satisfaccion", hue="Marca", data=data,
              palette='Set2',ax=ax1);
ax2 = plt.subplot(122)
sns.pointplot(x="Marca", y="Satisfaccion", hue="Condicion", data=data,
              palette='Set2',ax=ax2);

plt.show()

res = model.resid
fig = sm.qqplot(res, line='s')
plt.show()