import pandas as pd
from sklearn.metrics import confusion_matrix
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import cohen_kappa_score


def function_ejercicio2(datos1_name,datos2_name):
	datos = pd.read_excel('datos/Datos_Concordancia_ej_2.xlsx')

	print "Las columnas del archivo son:"
	print datos.columns
	Data1 = datos1_name
	Data2 = datos2_name
	labels = list(set(np.hstack([datos[Data1].values,datos[Data1].values])))
	print 'Los niveles totales son:'
	print labels
	cm1 = confusion_matrix(datos[Data1].values,datos[Data2].values,labels)
	cm1 = np.asarray(cm1)
	#cm1 = cm1[::-1,:][:,::-1]
	cm1 = np.hstack([cm1, np.zeros((np.shape(cm1)[0],1))])
	cm1 = np.vstack([cm1, np.zeros(np.shape(cm1)[1])])

	cant_labels=len(labels)
	for i,val in enumerate(labels):
		cm1[i,cant_labels]=np.sum(cm1[i,:cant_labels])
		cm1[cant_labels, i] = np.sum(cm1[:cant_labels, i])
		cm1[cant_labels,cant_labels]= np.sum(cm1[:cant_labels, cant_labels])


	ax1 = plt.subplot(111)
	sns.heatmap(cm1, cbar=False, ax=ax1, yticklabels=labels+['Suma'],
					xticklabels=labels, annot=True,fmt='.0f')
	ax1.set_ylabel(Data1)
	ax1.set_xlabel(Data2)
	ax1.set_title('Matriz de concordancia entre %s y %s' %(Data1,Data2))

	print 'Kappa sin correccion por pesos:'
	print cohen_kappa_score(datos[Data1].values,datos[Data2].values)
	print 'Kappa con correccion por pesos lineal:'
	print cohen_kappa_score(datos[Data1].values,datos[Data2].values,weights='linear')
	print 'Kappa con correccion por pesos cuadratica:'
	print cohen_kappa_score(datos[Data1].values, datos[Data2].values, weights='quadratic')