import numpy as np
import pandas as pd
from statsmodels.formula.api import ols
import scipy.stats as stats
import seaborn as sns

datos = pd.read_excel('datos/Datos_Comp_metodos_ej_1_edited.xlsx')

print 'Correlacion lineal:'
print np.corrcoef(datos['Normal_pfm'],datos['Mini_pfm'])[0,1]

print '\nT-Test:'
print stats.ttest_rel(datos['Normal_pfm'],datos['Mini_pfm'])


print '\nModelo lineal(OLS):\n'

mod = ols('Normal_pfm ~ Mini_pfm',data=datos).fit()


print mod.summary2()

sns.lmplot('Normal_pfm','Mini_pfm',datos)