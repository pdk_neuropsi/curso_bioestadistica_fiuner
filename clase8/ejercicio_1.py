import pandas as pd
from sklearn.metrics import confusion_matrix
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np


def function_ejercicio1(datos1_name,datos2_name):
	datos = pd.read_excel('datos/Datos_Concordancia_ej_1.xlsx')


	Data1 = datos1_name
	Data2 = datos2_name
	cm1 = confusion_matrix(datos[Data1].values,datos[Data2].values,[0,1])
	cm1 = np.asarray(cm1)
	cm1 = cm1[::-1,:][:,::-1]
	cm1 = np.hstack([cm1, np.zeros((np.shape(cm1)[0],1))])
	cm1 = np.vstack([cm1, np.zeros(np.shape(cm1)[1])])
	cm1[0,2]=np.sum(cm1[0,:2])
	cm1[1, 2] = np.sum(cm1[1, :2])
	cm1[2, 0] = np.sum(cm1[:2, 0])
	cm1[2, 1] = np.sum(cm1[:2, 1])
	cm1[2,2]= np.sum(cm1[:2, 2])

	Po= 1.0*(cm1[0,0]+cm1[1,1])/cm1[2,2]
	Ppos=1.0*(2*cm1[0,0])/(cm1[2,0]+cm1[0,2])
	Pneg=1.0*(2*cm1[1,1])/(cm1[2,1]+cm1[1,2])
	Pe=1.0*(cm1[0,2]/cm1[2,2])*(cm1[2,0]/cm1[2,2])+1.0*(cm1[1,2]/cm1[2,2])*(cm1[2,1]/cm1[2,2])
	Kappa=(Po-Pe)/(1-Pe)


	print cm1

	ax1 = plt.subplot(111)
	sns.heatmap(cm1, cbar=False, ax=ax1, yticklabels=['Pos','Neg','Suma'],
					xticklabels=['Pos','Neg','Suma'], annot=True,fmt='.0f')
	ax1.set_ylabel(Data1)
	ax1.set_xlabel(Data2)
	ax1.set_title('Matriz de concordancia entre %s y %s' %(Data1,Data2))

	print 'Po= %f, Ppos = %f, Pneg= %f, Pe= %f, Kappa = %f '%(Po,Ppos,Pneg,Pe,Kappa)
