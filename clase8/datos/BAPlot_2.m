function stats = BAPlot_2(data)
% 
% This script was made to draw Bland-Altman plots and calculate limits of
% agreements. The script is based on Atkinson,G.; Nevill,A.M. 1998
% "Statistical methods for assessing measurement error (reliability) in
% variables relevant to sports medicine" in Sports Medicine
% and "Measuring agreement in method comparison studies" Bland Altman
% Statistical Methods in Medical Research 
% http://search.ebscohost.com/login.aspx?direct=true&db=aph&AN=4164675&site=ehost-live
%
% Input:
%       data is matrix of observations. Each row is an object of measurement and
%       each column is a judge or measurement. TWO COLUMNS ONLY!!!


%% Create figure and scatters the data

m1=data(:,1);
m2=data(:,2);

figure1 = figure('Color',[1 1 1]);
scatter (m1,m2)
%giving equal span on the axes
xlim([min([min(m1) min(m2)]) max([max(m1) max(m2)])]);
ylim([min([min(m1) min(m2)]) max([max(m1) max(m2)])]);
%draw the diagonal
line([min([min(m1) min(m2)]) max([max(m1) max(m2)])], [min([min(m1) min(m2)]) max([max(m1) max(m2)])])

%% Bland Alman plot and statistics


A = (m1+m2)/2;  % Calculates the Average (A)
D = (m2-m1);      % Claculates the Difference (D)
n = length(m1);

t = tinv(.975,n-1);
Bias = mean(D)
SEB = std(D)/sqrt(n) %the standard error of the bias or difference
CIB = [Bias-t*SEB Bias+t*SEB] % the 95% CI of the bias... or difference

LOA = [(mean(D)-1.96*std(D)) (mean(D)+1.96*std(D))] % limits of agreement
SELOA = 1.71*SEB
CILOA =[LOA'-t*SELOA LOA'+t*SELOA]'

stats.Bias = Bias;
stats.SEB = SEB;
stats.CIB = CIB;
stats.LOA = LOA;
stats.SELOA = SELOA;
stats.CILOA = CILOA;

% Create figure and scatters the A vs the D
figure1 = figure('Color',[1 1 1]);
% Create axes
axes1 = axes('Parent',figure1,'LineWidth',2,'FontSize',12,...
    'FontName','Arial');
hold(axes1,'all');
scatter (A,D,'MarkerFaceColor',[0 0 0],'MarkerEdgeColor',[0 0 0],...
    'Marker','x',...
    'LineWidth',1);
hold on

%creates lines
line([min(A) max(A)],[0 0],'LineStyle','--','LineWidth',2,'Color',[0 0 0])
line([min(A) max(A)],[ mean(D) mean(D)],'LineWidth',2,'Color',[0 0 0])
line([min(A) max(A)],[LOA(1) LOA(1)],'LineWidth',2,'Color',[0 0 0])
line([min(A) max(A)],[LOA(2) LOA(2)],'LineWidth',2,'Color',[0 0 0])

% Create xlabels
text(max(A),mean(D),'Mean','FontSize',12,'FontName','Arial')
text(max(A),LOA(1),'Mean - 1.96 SD','FontSize',12,'FontName','Arial')
text(max(A),LOA(2),'Mean + 1.96 SD','FontSize',12,'FontName','Arial')
xlabel('Average','FontSize',14,'FontName','Arial');
ylabel('Difference (m2-m1)','FontSize',14,'FontName','Arial');
title('Bland Altman Plot','FontSize',14,'FontName','Arial');
xlim(axes1,[floor(min(A)) max(A)+1.5])

%% Looking at heteroscedacity

% Create figure and scatters the A vs the D
figure1 = figure('Color',[1 1 1]);
% Create axes
axes1 = axes('Parent',figure1,'LineWidth',2,'FontSize',12,...
    'FontName','Arial');
hold(axes1,'all');
scatter (A,abs(D),'MarkerEdgeColor',[0 0 0],...
    'Marker','o',...
    'LineWidth',2);%'MarkerFaceColor',[1 1 1]
hold on
xlabel('Average','FontSize',14,'FontName','Arial');
ylabel('Absolute Difference','FontSize',14,'FontName','Arial');
title('Heteroscedasticity?','FontSize',14,'FontName','Arial');
xlim(axes1,[floor(min(A)) max(A)+1.5])

%% Bland Alman plot with regression to account for non-uniform Differencess

fitobject = fit(A,D,'poly1');
b1 = fitobject.p1;              % The slope of the linear regression
b0 = fitobject.p2;              % The intersection of the linear regression
R = D-(b0+b1*A);                % The residuals of the regression
LOAR = [(mean(R)-2.46*std(R)) (mean(R)+2.46*std(R))]; % limits of agreement of the residuals
stats.LOAR = LOAR;

% Create figure and scatters the average vs the reciduals
figure1 = figure('Color',[1 1 1]);
scatter (A,R)
hold on

%creates lines
line([min(A) max(A)],[0 0],'LineStyle','--')
line([min(A) max(A)],[mean(R) mean(R)])
line([min(A) max(A)],[LOAR(1) LOAR(1)])
line([min(A) max(A)],[LOAR(2) LOAR(2)])

% Create xlabels
text(max(A),mean(D),'Mean')
text(max(A),LOAR(1),'Mean - 2.46 SD')
text(max(A),LOAR(2),'Mean + 2.46 SD')
xlabel('Average');
ylabel('Residuals');
title('Residual plot with limits of agreements');

% Create figure and scatters the A vs the D
figure1 = figure('Color',[1 1 1]);
scatter (A,D)
hold on
line([min(A) max(A)],[0 0],'LineStyle','--')
line([min(A) max(A)],b0+b1.*[min(A) max(A)])
line([min(A) max(A)],b0+b1.*[min(A) max(A)]-2.46*std(R))
line([min(A) max(A)],b0+b1.*[min(A) max(A)]+2.46*std(R))

% Create xlabels
text(max(A),b0+b1.*max(A),'Regresed mean')
text(max(A),b0+b1.*[max(A)]-2.46*std(R),'Lower 95% limit')
text(max(A),b0+b1.*[max(A)]+2.46*std(R),'Upper 95% limit')
xlabel('Average');
ylabel('Difference');
title('Bland Altman Plot with regression');
